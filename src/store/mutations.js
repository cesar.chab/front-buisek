  export const SET_CURRENT_USER = (state, { user }) => {
    state.user = user;
  };  
  export const SET_TOKEN = (state, { user }) => {
    state.token = user;
  };

  export const SET_VOTES = (state, { vote }) => {
    state.vote = vote;
  };
  