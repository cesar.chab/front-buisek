// Vuex
import Vue from "vue";
import Vuex from "vuex";
import createPersistedStore from "vuex-persistedstate";
import * as Cookies from "js-cookie";
import * as actions from "./actions";
import * as mutations from "./mutations";
import _ from "lodash";

Vue.use(Vuex);

export default new Vuex.Store({
  plugins: [
    createPersistedStore({
      getState: key => Cookies.getJSON(key),
      setState: (key, state) =>
        Cookies.set(key, state, {
          expires: parseInt(process.env.VUE_APP_VUEX_COOKIES_EXPIRE_TIME),
          secure: process.env.VUE_APP_VUEX_COOKIES_SECURE === "true"
        })
    })
  ],
  state: {
    token: null,   
    user: null,  
    vote: null
  },
  tableFilters: null,
  actions,
  getters: {
    getTokens: state => {
      return state.token.access_token;
    },   
    getUserId: state => {
      return state.user.dni;
    },    
    getUserName: state => {
      return state.user.name;
    },
    getVotes: state => {
      return state.vote
    }
  },
  mutations,
  modules: {}
});
