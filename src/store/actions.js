import router from "@/router";
import AuthProvider from "@/providers/Auth";
import BlockchainProvider from "@/providers/Blockchain";
import _ from "lodash";
import store from ".";

const AuthResource = new AuthProvider();
const BlockchainResource = new BlockchainProvider();


export const GET_AUTH_TOKEN = ({ commit, state }, formdata) => {
  return new Promise((resolve, reject) => {
    AuthResource.login(formdata).then((response) => {
        if (response.data) {
            commit("SET_TOKEN", {
              user: {
                access_token: response.data.token                
              }
            });

            commit("SET_CURRENT_USER", {
                user: {
                  dni: response.data.dni,
                  name: response.data.name                  
                }
            });
            
            localStorage.setItem("loggedIn", true);
            localStorage.setItem("token", response.data.token);
            return BlockchainResource.getBlocks();
        }
      }).then(votes => {
        commit("SET_VOTES", {
          vote: votes.data
        });
        resolve(true);
      })
      .catch((error) => {
        localStorage.setItem("loggedIn", false);
        reject(error);
      });
  });
};

export const VOTES = ({ commit, state }) => {
  return new Promise((resolve, reject) => {
    BlockchainResource.getBlocks().then(response => {
      // console.log(response.data)
      commit("SET_VOTES", {
        vote: response.data
      });
      resolve(true);
    }).catch(error => {
      console.log(error)
      reject(error)
    });
  }); 
};

export const LOGOUT = ({ commit, state }) => {
  localStorage.clear();
  commit("SET_TOKEN", { token: null });
  commit("SET_CURRENT_USER", { user: null });
  commit("SET_VOTES", { vote: null});
  router.push({name: "Index"})
};