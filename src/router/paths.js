export default [
  {
    path: '*',
    meta: {
      public: true,
    },
    redirect: {
      path: '/404'
    }
  },  
  {
    path: '/404',
    meta: {
      public: true,
    },
    name: 'NotFound',
    component: () => import(
      `@/views/NotFound.vue`
    )
  },
  {
    path: '/403',
    meta: {
      public: true,
    },
    name: 'AccessDenied',
    component: () => import(
      `@/views/Deny.vue`
    )
  },
  {
    path: '/500',
    meta: {
      public: true,
    },
    name: 'ServerError',
    component: () => import(
      `@/views/Error.vue`
    )
  },
  {
    path: '/login',
    meta: {
      public: true,
    },
    name: 'Login',
    component: () => import(
      `@/views/Login/Login.vue`
    )
  },
  {
    path: '/',
    meta: { },
    name: 'Root',
    redirect: {
      name: 'Index'
    }
  },
  {
    path: '/index',
    meta: {
      public: true,
    },
    name: 'Index',
    component: () => import(
      `@/views/Index.vue`
    )
  },
  {
    path: '/dashboard',
    meta: { breadcrumb: true },
    name: 'Dashboard',
    component: () => import(
      `@/views/Dashboard/Dashboard.vue`
    )
  },
  {
    path: '/votes',
    meta: { breadcrumb: true },
    name: 'Votes',
    component: () => import(
      `@/views/Votes/Votes.vue`
    )
  },
];
