import Vue from "vue";
import VueRouter from "vue-router";
import store from "@/store";
import paths from "./paths";
import NProgress from "nprogress";
import "nprogress/nprogress.css";
import _ from "lodash";

import AuthProvider from "@/providers/Auth";
import momentTimezone from "moment-timezone";
const authResource = new AuthProvider();

Vue.use(VueRouter);
const router = new VueRouter({
  base: "/",
  mode: "hash",
  linkActiveClass: "active",
  routes: paths,
});

// router gards
router.beforeEach((to, from, next) => {
  NProgress.start();
  // if (to.name === "Votes" || to.name === "Dashboard") {
  //   if (!window.localStorage.voted || window.localStorage.voted === undefined) {
  //     router.push({name: 'Login'});
  //   } else {
  //     if (window.localStorage.voted || window.localStorage.voted !== undefined) {
  //       router.push({ name: "Dashboard" });
  //     } else {
  //       router.push({name: 'Votes'});
  //     }
  //   }
  // } else
  // if(!window.localStorage.loggedIn || window.localStorage.loggedIn === undefined) {
  //   router.go({path: '/index'});
  //   // // store.dispatch("LOGOUT");
  //   // next({
  //   //   path: '/'
  //   // })
  //   router.push({name: 'Index'});
  // } else
 if (to.name === "Login") {
    if (store.state.user === null) {
      next();
    } else {
      if (
        window.localStorage.voted ||
        window.localStorage.voted !== undefined
      ) {
        router.push({ name: "Dashboard" });
      } else {
        router.push({ name: "Votes" });
      }
    }
  } else if (!store.state.user) {
    if (
      !window.localStorage.loggedIn ||
      window.localStorage.loggedIn === undefined
    ) {
      next();
    } else {
      store.dispatch("LOGOUT");
    }
  } else {
    next();
  }
});

router.afterEach((to, from) => {
  // ...
  NProgress.done();
});

export default router;
