import axios from "axios";

export default class Auth {
  /**
   * Function to login app
   * @param {*} formdata
   * @return Promise
   */
  login(formdata) {
    return axios.post(`${process.env.VUE_APP_API_URL}/auth`, formdata);
  } 
}
