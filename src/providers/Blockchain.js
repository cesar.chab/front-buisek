import axios from "axios";

export default class Blockchain {

  /**
   * Function to get blocks
   * @return Promise
   */
  getBlocks() {
    return axios.get(`${process.env.VUE_APP_API_URL}/blocks`);
  } 

   /**
   * Function to mine
   * @return Promise
   */
  mine(formdata) {
    return axios.post(`${process.env.VUE_APP_API_URL}/mine`, formdata);
  } 
}
