import Vue from "vue";
import App from "./App.vue";
import i18n from "./i18n";
import vuetify from "vuetify";
import "font-awesome/css/font-awesome.css";
import "./theme/default.styl";
import colors from "vuetify/es5/util/colors";
import Truncate from "lodash.truncate";
import VeeValidate, {Validator} from "vee-validate";
import store from "./store";
import router from "./router";
import FlagIcon from 'vue-flag-icon';
import valEN from 'vee-validate/dist/locale/en';
import valES from 'vee-validate/dist/locale/es';
// import VueEcho from 'vue-echo-laravel';



//Components 
import en from 'vuetify/lib/locale/en.js'
import es from 'vuetify/lib/locale/es.js'

//Validator
import validationMessagesEn from 'vee-validate/dist/locale/en';
import validationMessagesEs from 'vee-validate/dist/locale/es';

//Sweet Alert
import VueSweetalert2 from 'vue-sweetalert2';

import "./interceptors";

let langVAL = {'es': valES, 'en': valEN}
let langEUI = {'es': es, 'en': en}

Vue.use(FlagIcon);
Vue.config.productionTip = false;
// Validator.localize(store.getters.getLocale, langVAL[store.getters.getLocale])

Vue.use(VueSweetalert2);

// Vue.use(VueEcho, {
//   broadcaster: 'socket.io',
//   host: window.location.hostname + ':6001',
// });

// Vue dynamic locale
Vue.prototype.$locale = {
  change(locale) {
    i18n.locale = locale;
    // Validator.localize(locale, langVAL[locale])
    Vue.use(vuetify, {
      lang: {
        locales: {
          es,
          en
        },
        current: locale
      },
    })
  },
  current() {
    return i18n.locale;
  }
};

// Helpers
// Global filters
Vue.filter("truncate", Truncate);
Vue.use(VeeValidate, {
  i18nRootKey: 'validations', // customize the root path for validation messages.
  i18n,
  dictionary: {
    en: validationMessagesEn,
    es: validationMessagesEs
  },
  fieldsBagName: "formFields" 
});

Vue.use(vuetify, {
  lang: {
    locales: {
      es,
      en
    },
    current: i18n.locale
  },
  theme: {
    primary: colors.indigo.base, // #E53935
    secondary: colors.indigo.lighten4, // #FFCDD2
    accent: colors.indigo.base, // #3F51B5,
  },
  options: {
    themeVariations: ["primary", "secondary", "accent"],
    extra: {
      mainToolbar: {
        color: "primary"
      },
      sideToolbar: {},
      sideNav: "primary",
      mainNav: "primary lighten-1",
      bodyBg: ""
    }
  }
});

new Vue({
  i18n,
  vuetify,
  store,
  router,
  render: h => h(App)
}).$mount("#app");
