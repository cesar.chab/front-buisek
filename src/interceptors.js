import _ from "lodash";
// import router from './router'
import store from "./store";
import axios from "axios";
import * as Cookies from "js-cookie";

const excludePaths = ["/login", "/refresh_token"];

// Interceptor for requests
axios.interceptors.request.use(
  config => {
    const token = localStorage.getItem("token");
    config.headers = config.headers || {};
    if (!_.includes(excludePaths, config.url)) {
      // config.headers["Content-Type"] = "application/json";
      // config.headers["X-Requested-With"] = `XMLHttpRequest`;
      // config.headers.Authorization = `Bearer ${token}`;
      // config.headers["Accept-Language"] = store.getters.getLocale.substring(
      //   0,
      //   2
      // ); // Workaround for translate on backend
    }
    return config;
  },
  err => {
    return Promise.reject(err);
  }
);

const responseStatuses = err => {
  if (_.includes([409, 404], err.response.status)) {
    throw err;
  } else if (
    (err.response.status === 401 || err.response.status === 403) &&
    err.response.config &&
    !err.response.config.__isRetryRequest
  ) {
    if (!window.localStorage.loggedIn && window.localStorage.loggedIn != undefined) {      
      Cookies.remove("vuex");
      store.dispatch("LOGOUT");
    }
  } else {
    return Promise.reject(err);
  }
  throw err;
};

//Interceptor for responses
axios.interceptors.response.use(
  response => {
    return response;
  },
  err => {
    if (err.response.config) {
      if (!_.includes(excludePaths, err.response.config.url)) {
        return responseStatuses(err);
      }
    } else {
      return responseStatuses(err);
    }
  }
);
