# frontend

## Project setup
```
npm install

```

## Change url API
```
VUE_APP_API_URL = http://localhost:3000
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
